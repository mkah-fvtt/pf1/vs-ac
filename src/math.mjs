import { CFG } from './common.mjs';

/**
 * @param {Token} token
 */
const tokenMargin = (token) => Math.floor(Math.min(token.width, token.height) / 2);

/* global Ray */

/**
 * @param {Ray} ray
 * @returns {number}
 */
export const normAngle = (ray) => {
	const a = ray.angle % (2 * Math.PI);
	return a < 0 ? a + 2 * Math.PI : a;
};

class RayExt {
	ray;
	#scene;

	/**
	 * @param {Ray} ray
	 * @param {Scene} scene
	 */
	constructor(ray, scene) {
		this.ray = ray;
		this.#scene = scene;
	}

	/** @type {number} - Raw distance */
	get distance() { return this.ray.distance; }

	/** @type {number} - Distance in grid cells */
	get cells() { return this.ray.distance / this.#scene.grid.size; }

	/** @type {number} - Distance in units (Feet, meters, whatever) */
	get units() { return this.cells * this.#scene.grid.distance; }

	get normAngle() { return normAngle(this.ray); }
}

/**
 * @param {Point} pointA
 * @param {Point} pointB
 * @param {Scene} scene
 * @returns {RayExt}
 */
export const getRay = (pointA, pointB, scene) => new RayExt(new Ray(pointA, pointB), scene);

/**
 * Artificial actor height. Assume it is the larger of width and height.
 *
 * @param {TokenDocument} token
 */
const actorHeight = (token) => Math.max(token.height, token.width);

// const badSector = new Sector(-1, NaN, NaN, 'ERR');

const directionLabel = { M: 'Middle', T: 'Top', B: 'Bottom', L: 'Left', R: 'Right' };
const directionSymbol = '→↘↓↙←↖↑↗';

const sectors = {};

function sectorName(sector, id) {
	if (CFG.debug.verbose) console.debug('%cVS AC%c | sectorName:', CFG.COLORS.main, CFG.COLORS.unset, sector, id ?? directionSymbol[sector]);
	const s = id ?? sectors[sector]?.id;
	if (!s) return `ERROR[${sector}]`;
	const [vpos, hpos] = s;
	return `${directionLabel[vpos]} ${directionLabel[hpos]} ${directionSymbol[sector]}`;
}

class Sector {
	sector;
	x;
	y;
	id;
	label;
	constructor({ sector, x, y, id, label } = {}) {
		this.sector = sector;
		this.x = x;
		this.y = y;
		this.id = id;
		this.label = label ?? sectorName(sector, id);
	}

	clone() {
		const { sector, x, y, id, label } = this;
		return new Sector({ sector, x, y, id, label });
	}
}

/**
 * 45 degree sectors
 * 0 = R, 1 = BR, 2 = B, 3 = BL, 4 = L, 5 = TL, 6 = T, 7 = TR
 */
sectors[0] = new Sector({ sector: 0, x: 0.5, y: 0, id: 'MR' }); // middle right
sectors[1] = new Sector({ sector: 1, x: 0.5, y: 0.5, id: 'BR' }); // bottom right,
sectors[2] = new Sector({ sector: 2, x: 0, y: 0.5, id: 'BM' }); // bottom middle,
sectors[3] = new Sector({ sector: 3, x: -0.5, y: 0.5, id: 'BL' }); // bottom left,
sectors[4] = new Sector({ sector: 4, x: -0.5, y: 0, id: 'ML' }); // middle left,
sectors[5] = new Sector({ sector: 5, x: -0.5, y: -0.5, id: 'TL' }); // top left,
sectors[6] = new Sector({ sector: 6, x: 0, y: -0.5, id: 'TM' }); // top middle,
sectors[7] = new Sector({ sector: 7, x: 0.5, y: -0.5, id: 'TR' }); // top right,

const pi2 = Math.PI * 2;
const sectorAngleDeg = 360 / 8; // 8 sectors
const sectorAngleRad = pi2 / 8; // 8 sectors
const wrapDeg = (d) => d >= 360 ? d - 360 : d;
const wrapRad = (r) => r >= pi2 ? r - pi2 : r;
const wrapSector = (s) => s >= 8 ? 0 : s;

/**
 * Get 45 degree sector number, 0 to 7 clockwise, starting from right.
 *
 * @param {number} radian Radian to convert into sector
 * @returns {number} Sector
 */
export const getSector = (radian) => {
	const angleDeg = wrapRad(radian),
		sector = wrapSector(Math.round(angleDeg / sectorAngleRad));
	// console.log('rad:', Number(radian.toFixed(3)), '-> deg:', Number(angleDeg.toFixed(2)), '= sector:', sector, `[${sectorName(sector)}]`);

	if (CFG.debug.verbose) console.debug('%cVS AC%c | getSector:', CFG.COLORS.main, CFG.COLORS.unset,
		{ radian: Math.roundDecimals(radian, 2), sectorN: sector, sectorL: sectorName(sector) });
	return sector;
};

// new Token(tokenData) loses tokenData's x/y coordinates
const sectorAdjust = (sector, token, origin) => {
	const adjust = sectors[sector];
	// console.log(sector, adjust, token, origin);
	return {
		x: adjust.x * token.w + origin.x,
		y: adjust.y * token.h + origin.y,
	};
};

function debugRay(rd, label) {
	console.group(label);
	console.log('Distance:', Number(rd.unitDistance.toFixed(2)), 'units;', Number(rd.cellDistance.toFixed(2)), 'cells');
	console.groupCollapsed('Data');
	console.log('Origin:', rd.ray.A);
	console.log('Target:', rd.ray.B);
	console.log('Object:', rd.ray);
	console.groupEnd();
	console.groupEnd();
}

/**
 * Create matrix(?) of sub-cells from token.
 *
 * @param {Token} token
 */
export function tokenSquares(token) {
	const cells = [],
		scale = token.scene.grid.size,
		doc = token.document,
		w = doc.width, // Number of cells
		h = doc.height, // number of cells
		{ x, y } = token.position,
		origin = token.center;

	const boxIt = (_x, _y, s) => {
		return {
			x: x + scale * _x,
			y: y + scale * _y,
			sector: s,
			get center() {
				return { x: this.x + scale / 2, y: this.y + scale / 2 };
			},
		};
	};

	for (let h0 = 0; h0 < h; h0++) {
		cells[h0] = [];
		for (let w0 = 0; w0 < w; w0++) {
			const hE = w0 == 0 || w0 == w - 1, // horizontal edge
				vE = h0 == 0 || h0 == h - 1, // vertical edge
				box = boxIt(w0, h0);
			if (hE || vE) {
				const ray = new Ray(origin, box.center);
				box.sector = getSector(normAngle(ray));
				box.sectorName = sectors[box.sector].label;
				box.ray = ray;
			}
			cells[h0][w0] = box;
		}
	}
	return cells;
}

const debugToken = (token, { sqsm, ddsqsm, nbsqsm, sqsf } = {}) => {
	console.group('Token:', token.name);
	console.log({ token });
	console.log('Size:', {
		w: token.w,
		h: token.h,
	});
	console.log('TopLeft:', {
		x: token.x,
		y: token.y,
	});
	console.log('BottomRight:', {
		x: token.x + token.w,
		y: token.y + token.h,
	});

	if (sqsf.length === 1)
		console.log('Lone Cell:', sqsf[0]);

	console.log('Cells in Desired Sector:', ddsqsm);
	console.log('+Neigbours:', nbsqsm);
	console.log({ sqsm });

	console.groupEnd();
};

/**
 * Get array of cells within a sector.
 *
 * @param {Token} token
 * @param sector
 */
function getTokenSquaresWithinSector(token, sector) {
	const sqs = tokenSquares(token); // all squares
	// Reduce number of returned squares to only mostly relevant ones
	const sqsf = sqs.flat();

	// Adjust singular cell to match desired sector and return it; this could be returned already for no notable benefit
	if (sqsf.length === 1)
		sqsf[0].sector = sector;

	// Get cells within desired sector
	let sqsm = sqsf.filter(c => c.sector === sector);
	const ddsqsm = CFG.debug.math ? foundry.utils.deepClone(sqsm) : null;
	let nbsqsm;

	// Get neighbouring sectors if no matches found
	if (sqsm.length === 0) {
		const neighbours = [wrapSector(sector - 1), wrapSector(sector + 1)];
		sqsm = sqsf.filter(c => neighbours.includes(c.sector));
		nbsqsm = CFG.debug.math ? foundry.utils.deepClone(sqsm) : null;
	}

	if (CFG.debug.math)
		debugToken(token, { sqsm, sqsf, ddsqsm, nbsqsm });

	return sqsm;
}

/**
 * Calculates distances between two tokens in
 *
 * @param {Token} sourceToken
 * @param {Token} targetToken
 * @param {Scene|null} scene Scene on which the tokens are. Retrieved from first token if not present.
 * @returns {object}
 */
export function tokenSubcellDistance(sourceToken, targetToken, scene) {
	scene ||= sourceToken.scene;
	if (!scene) return;

	if (CFG.debug.basic) console.debug('%cVS AC%c | tokenSubcellDistance:', CFG.COLORS.main, CFG.COLORS.unset, { source: sourceToken, target: targetToken });

	const origin = { x: sourceToken.center.x, y: sourceToken.center.y };
	const destination = { x: targetToken.center.x, y: targetToken.center.y };

	const rd = getRay(origin, destination, scene);

	const originSector = getSector(rd.normAngle),
		targetSector = getSector(rd.normAngle + Math.PI); // reverse sector

	if (CFG.debug.basic) console.debug('Connecting sectors: [', sectorName(originSector), '] to [', sectorName(targetSector), ']');

	// Get squares for both tokens
	const sSqs = getTokenSquaresWithinSector(sourceToken, originSector),
		tSqs = getTokenSquaresWithinSector(targetToken, targetSector);

	// Find shortest ray
	let shortest, newOrigin, newDest;
	for (const sq0 of sSqs) {
		for (const tsq0 of tSqs) {
			const ray = getRay(sq0, tsq0, scene);
			if (CFG.debug.math) console.debug(ray.distance, { sq0, tsq0 }, ray);
			if (ray.distance < (shortest?.distance ?? Number.POSITIVE_INFINITY)) {
				shortest = ray;
				newOrigin = sq0;
				newDest = tsq0;
			}
		}
	}

	return { adjusted: shortest, origin: newOrigin, destination: newDest };
}

const get5105Fudge = (diagonals, straights) => Math.floor(diagonals * 1.5 + straights);

// 5105 rule handler
const get5105Distance = (x, y) => {
	const xd = Math.ceil(Math.abs(x)), // x distance
		yd = Math.ceil(Math.abs(y)), // y distance
		diagonals = Math.min(xd, yd),
		straights = Math.abs(yd - xd),
		fudged = get5105Fudge(diagonals, straights);
	return { fudged, diagonals, straights };
};

// 5105 rule handler
const get5105DistanceFromRay = (rd, scale) => get5105Distance(rd.ray.dx / scale, rd.ray.dy / scale);

/**
 * Distance
 *
 * @param {TokenDocument} attacker
 * @param {TokenDocument} target
 * @param {Scene} scene
 */
export function tokenDistance(attacker, target, scene) {
	const gridScale = scene.grid.distance,
		attackerE = attacker.elevation,
		elevationDiff = target.elevation - attackerE;

	// 2D distance to target
	const { adjusted, origin, destination } = tokenSubcellDistance(attacker.object, target.object);
	const distance2D = adjusted.units;

	// Apply 5105 rule
	const rcfg = game.settings.get(CFG.id, CFG.SETTINGS.rangeConfig),
		pf5105 = rcfg.pf5105 ? get5105DistanceFromRay(adjusted, scene.grid.size) : null,
		actualDistance = rcfg.pf5105 ? pf5105.fudged * gridScale : distance2D,
		distance3D = Math.hypot(actualDistance, elevationDiff);

	return {
		distance: distance3D,
		adjust: tokenMargin(attacker.object) + tokenMargin(target.object),
		origin,
		destination,
	};
}

/**
 * @param {TokenDocument} attacker
 * @param {TokenDocument} target
 * @param {Scene} scene
 */
export const sceneTokenDistance = (attacker, target, scene) => {
	if (scene.grid.type === 1) return tokenDistance(attacker, target, scene);
	else {
		// Center to center in anything else because everything else depends on canvas scale
		// TODO: Reduce by distance to the edge of the token.
		const rd = getRay(attacker.object.center, target.object.center, scene);
		console.debug('UnsupportedGrid:', { rd });
		return {
			distance: rd.units,
			adjust: tokenMargin(attacker.object) + tokenMargin(target.object),
			destination: rd.ray.B,
			origin: rd.ray.A,
		};
	}
};
