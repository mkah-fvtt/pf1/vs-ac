// import { CFG } from './common.mjs';

/**
 * @param {TokenDocument} doc
 * @param {object} options
 * @param {boolean} options.pan Pan to token
 * @param {boolean} options.select Select token (and release any other previously selected tokens)
 * @param {boolean} options.swapScene Swap scene to where the token is
 * @param {boolean} options.releaseOthers
 */
export async function focusToken(doc, { pan = true, select = true, swapScene = true, releaseOthers = true } = {}) {
	// const focusCfg = game.settings.get(CFG.module, CFG.SETTINGS.focusKey);

	// Swap scene
	// if (swapScene) await doc.parent?.view();

	// If wrong scene, .object will be null
	if (canvas.scene.id !== doc.parent.id) {
		return void ui.notifications.warn('VS-AC.Error.WrongScene', { localize: true });
	}

	const token = doc?.object;
	if (!token) return; // Bad data

	if (!token.isVisible) return; // Don't deal with invisible tokens

	// Center token
	if (pan) canvas.animatePan({ x: token.center.x, y: token.center.y, duration: 250 });
	// Take control of it
	if (select) token.control({ releaseOthers });
}
