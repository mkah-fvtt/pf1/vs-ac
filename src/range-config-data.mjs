export class RangeConfigData extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			pf5105: new fields.BooleanField({ initial: true }),
			display: new fields.BooleanField({ initial: true }),
		};
	}
}
