import { CFG } from './common.mjs';
import { RangeConfigData } from './range-config-data.mjs';

export class RangeConfigDialog extends FormApplication {
	settings;
	constructor(...args) {
		super(...args);
		this.settings = game.settings.get(CFG.id, CFG.SETTINGS.rangeConfig);
	}

	get template() {
		return `modules/${CFG.id}/template/range-config.hbs`;
	}

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			title: game.i18n.localize('VS-AC.Dialog.Range.Title'),
			classes: ['vs-ac', 'vs-ac-config'],
		});
	}

	getData(...args) {
		const data = super.getData(...args);
		data.settings = this.settings;
		return data;
	}

	_updateObject(ev, data) {
		this.settings = new RangeConfigData();
		this.settings.updateSource(data);
		game.settings.set(CFG.id, CFG.SETTINGS.rangeConfig, this.settings.toObject());
	}

	activateListeners(jq) {
		super.activateListeners(jq);
		jq.find('input[type="reset"]').on('click', (ev) => {
			ev.preventDefault();
			this.settings = new RangeConfigData();
			this.render(true);
		});
	}
}
