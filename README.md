# vs AC – for Pathfinder 1e (Experimental)

Shows target name, AC and range to them as part of attack chat cards.

Attack - AC difference is displayed next to the attack label for quick resolution of post roll modifiers (e.g. prone, combat stamina use, high ground, etc.).

- You can click on the target info to focus them.
- You can also click on the AC/Touch/FF section to cycle which the comparison is made against.

  ✅ It's okay. The AC diff makes post-attack math easier.

## Screencaps

![Screen capture](./img/screencap.png)

## Legend

- Range is as per scene configuration units.
- AC is Normal / Touch / FF.

FF is here used as short-hand for _Dex denied to AC_ like with the PF1 sheet.

## Known flaws

- Token rotation is not taken into account.
- Distance when elevation is involved does not take token height into consideration.
- 5105 rule (when enabled) is not considered in terms of elevation.
- Multiple instances of linked token on same scene will misbehave as the attacks have no token information in their case (this is Foundry limitation).
- Token visibility per normal player is tested when chat message is rendered, thus becoming dated for players.

## Install

Manifest URL: <https://gitlab.com/mkah-fvtt/pf1/vs-ac/-/releases/permalink/latest/downloads/module.json>

Foundry v11–v10 manifest: <https://gitlab.com/koboldworks/pf1/vs-ac/-/releases/1.0.0.1/downloads/module.json>

Foundry v9 manifest: <https://gitlab.com/mkah-fvtt/pf1/vs-ac/-/raw/0.3.1/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
