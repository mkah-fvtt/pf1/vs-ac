# Change Log

## 1.1.0

- Foundry v12 compatibility. V11 and older support dropped.

## 1.0.0

- Fix: Tokens with no actor would behave strangely.
- Change: Required permission level to see AC diff reduced to Observer from Owner.

## 0.5.3

- Fix: Overrides for Foundry v11's too generic stylings interfering with display.
- Fix: Natural roll detection could potentially be off due to Foundry bugs.
- i18n: Improve translation support.
- New: Transparency setting is now selection of 3 different options instead of a toggle.
  - Old options are migrated to `none` and `name` options.
  - New `full` option added to display same info to players as the GM.

## 0.5.2

- Fix: Error if any of the targeted tokens lacked actor.
- Change: Allow actorless tokens to appear as targets.
- New: Support PF1's touch attack option (PF1 0.83.0)

## 0.5.1

- New: Transparency setting. Disable to hide target name display from players.

## 0.5.0.1

- Fix: AC comparison was mislabeled as CMD comparison.

## 0.5.0

- Combat maneuvers display CMD/FFCMD comparison instead of AC/TAC/FFAC.
- Tooltips replaced by Foundry's tooltips.
- Module ID changed. Please manually uninstall the old.

## 0.4.0.1

- Fix for incorrect use of `pf1PreDisplayActionUse` hook parameters.

## 0.4.0

- Foundry v10 and PF 0.82.x compatibility (support for older versions dropped)
- Change: Release mechanism changed for smaller download and install size.

## 0.3.1

- Fix: Cycling AC caused token to be focused.
- Bundling via esbuild
- Pure CSS swapped for SCSS

## 0.3.0

- Note: Foundry v9 compatibility
- Fixed: Ray.normAngle no longer exists in Foundry, substitute applied.
- Added: Popped out chat messages now have click handling.
- Changed: Shift clicking targets no longer releases previously selected tokens, allowing easy multiselection.
- Added: Simple click eater implemented to avoid odd behavior with panning to a token while already panning.

## 0.2.6

- Dedicated repository. No notable functional changes.

## 0.2.5

- Touch attack handling improved. Cycling works better.
- Multitargeting improved.
  - Selected target shows normally, other targets are faded.
  - Selected AC comparison is clarified only for selected target.
  - Touch attacks immediately select touch AC when swapping targets.

## 0.2.4

- `touchAttack` boolean flag support.
- AC comparison cycling.

## 0.2.3.1 Hotfix for isOwner tests

## 0.2.3

- Attack - AC difference display & fix for token centering for unseen tokens

## 0.2.2.3 Minor fix for missing tokens

## 0.2.2.2 Scene acquisition fix

## 0.2.2.1 Use viewed scene instead of active

## 0.2.2

- Distance 5105 rule
- Subcell-based distance calculations

## 0.2.1.1 Another fix for linked tokens erroring

## 0.2.1

- Distance WIP & fix

## 0.2

- Multitargeting
- Distance display

## 0.1 Initial
